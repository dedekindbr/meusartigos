%% LyX 2.2.0 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[brazil]{paper}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage{textcomp}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{setspace}
\onehalfspacing

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\numberwithin{equation}{section}
\numberwithin{figure}{section}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage[alf]{abntex2cite}
\usepackage{titlesec}
\titleformat{\section}{\Large\bfseries\filcenter}{}{1em}{}
\usepackage{manyfoot}
\DeclareNewFootnote{B}[roman]

\makeatother

\usepackage{babel}
\begin{document}

\title{Abstração em Frege}

\author{Alessandro Bandeira Duarte\\Colóquio de Filosofia Moderna e Contemporânea:
Frege Filósofo\\ 2008}

\date{Maio de 2008}
\maketitle
\begin{abstract}
Nosso objetivo é apontar uma dificuldade interpretativa nos \textit{Fundamentos
da Aritmética} (1884) relacionada\footnoteB{teste} com uma certa
tensão entre um dos princípios fundamentais do livro – o princípio
do contexto – e o procedimento de Frege na §68, onde ele introduz
as extensões de conceitos e define explicitamente o operador-cardinalidade
por meio delas.
\end{abstract}

\section*{I}

Como é bem conhecido, pelo menos até 1903\footnote{De acordo com \citeonline[pp. 5-6]{Dummett1991}, Frege parece ter
percebido que a sua solução ao Paradoxo de Russell não funcionava
em 1906. Não podemos inferir daí que este foi o ano em que Frege desistiu
do logicismo. De fato, é somente em 1924/25, nos artigos póstumos
“Numbers and Arithmetic” \cite[p. 277]{Frege1979} e “A New Attempt
at a Foundation for Arithmetic” \cite[p. 278]{Frege1979}, que Frege
explicitamente abandona o seu logicismo. É difícil avaliar exatamente
quando Frege desistiu do logicismo. Relatos de Carnap, que foi seu
aluno até 1914, mostram que Frege, ainda nessa época, sustentava que
a aritmética poderia ser derivada por meios lógicos. Por exemplo,
no livro \textit{Mein Weg in die Philosophie}, Carnap escreve: “No
fim do semestre (de 1913), Frege chamou a atenção de que a nova lógica,
a qual ele nos apresentou, poderia servir para construir a matemática
inteira” \cite[p. 8, minha tradução]{Carnap1999}.}, ano da publicação do segundo volume de \textit{Grundgesetze der
Arithmetik} (GGA), Frege defendeu o logicismo em relação à aritmética,
a tese segundo a qual as leis básicas desta ciência poderiam ser provadas
a partir de definições lógicas de conceitos aritméticos, axiomas lógicos
e regras de inferências que preservam a verdade.

O logicismo de Frege, assim pensamos, é uma consequência natural do
processo de aritmetização da análise que ocorreu no século XIX. A
idéia de muitos matemáticos desse período era tornar a análise um
campo matemático autônomo\footnote{Veja \citeonline{Demopoulos1995b}}.
Até então, os teoremas dessa ciência eram “provados” recorrendo-se
às evidências ou às intuições geométricas. Nesse sentido, a análise
era dependente ou parasitária da geometria\footnote{A seguinte passagem de \citeonline[pp. 1-2]{Dedekind1963} é reveladora:
“As professor in the Polytechnic School in Zürich I found myself for
first time obliged to lecture upon the elements of the differential
calculus and felt more keenly than ever before the lack of a really
scientific foundation for arithmetic. In discussing the notion of
the approach of a variable magnitude to a fixed limiting value, and
especially in proving the theorem that every magnitude which grows
continually, but not beyond all limits, must certainly approach a
limiting value, \textit{I had recourse to geometric evidences}…For
myself this feeling of dissatisfaction was so overpowering that I
made the fixed resolve to keep meditating on question till I should
find a purely arithmetical and perfectly rigorous foundation for the
principles of infinitesimal analysis”.}.

Uma das primeiras tentativas feitas para mostrar que a análise não
dependia de intuições geométricas (ou de qualquer intuição) foi a
de Bernard Bolzano\footnote{Poderíamos mencionar também Cauchy.},
quando, em 1817, ele provou, analiticamente, baseado apenas na sua
definição de continuidade, o Teorema do Valor Intermediário\footnote{Esse teorema afirma o seguinte: sejam $y=f(x)$ uma função contínua
– intuitivamente, uma função contínua é aquela na qual qualquer modificação
no imput (no caso, $x$) leva a uma modificação no nosso output (no
caso, o valor da função $y$ em $x$) – sobre um intervalo fechado
de números reais $[a, b]$ e N um número entre os valores da função
$f(a)$ e $f(b)$, então existe um número $c$ que pertence ao intervalo
$[a, b]$, tal que o valor da função em $c$ é N.} \footnote{Bolzano era crítico à idéia de Kant de que a matemática era sintética
a priori. Um dos seus objetivos ao provar o Teorema do Valor Intermediário
por meios analíticos era mostrar que a intuição não desempenha qualquer
papel nas provas dos teoremas da análise. O leitor interessado pode
ler o artigo “Kant, Bolzano and the Emergence of logicism” (1982)
de Albert Coffa.}. Podemos citar muitos outros matemáticos engajados nesse processo:
Weierstrass\footnote{Weierstrass provou, analítica e independentemente, o Teorema do Valor
Intermediário que, inicialmente, recebeu o nome Teorema de Weierstrass.
Contudo, com a redescoberta da prova de Bolzano, este teorema é conhecido
hoje por Teorema de Bolzano-Weierstrass.} e Cantor.

Outra questão importante nesse processo de aritmetização da análise
era definir os números reais por meios puramente aritméticos. Além
disso, era necessário, a partir dessa definição, provar as propriedades\footnote{A propriedade mais importante é que os números reais formam um conjunto
contínuo, sem lacunas. Essa propriedade não é satisfeita pelos números
racionais. Estes, como os reais, formam um conjunto denso, ou seja,
dados quaisquer dos números racionais $a$ e $b$, existe sempre um
número racional $c$ entre eles, o que implica a existência de infinitos
números entre $a$ e $b$. Contudo, o conjunto dos racionais tem lacunas,
“buracos”. Por exemplo, $\surd2$ não corresponde, como já provado
há cerca de 2400 anos atrás, a nenhum número racional. } pertencentes a estes números sem recorrer à geometria. Em seu livreto,
\textit{Continuity and Irrational Numbers} (1872), Dedekind assume
os números racionais e suas propriedades como dadas e, a partir daí,
ele mostra como construir (ou criar, sua palavra preferida) os números
irracionais (cuja união com racionais produz os números reais). Não
é nosso objetivo entrar nos detalhes do procedimento de Dedekind aqui.
O que queremos enfatizar é que a aritmetização sugerida por Dedekind
nesse artigo só poderia ser bem-sucedida, se os números racionais
pudessem ser definidos e suas propriedades pudessem ser derivadas
por meios puramente aritméticos. Em outras palavras, se a definição
de número racional e suas propriedades dependessem da intuição geométrica
(ou de qualquer outro tipo de intuição), o processo de aritmetização
da análise estaria fadado ao fracasso. De alguma forma, a análise
dependeria da geometria (ou de algum tipo de intuição) e não seria
uma ciência autônoma.

Por sua vez, os números racionais podem ser definidos por meio puramente
aritméticos a partir dos números naturais. Há algumas formas de se
proceder. Primeiro, poderíamos definir os números inteiros como sendo
certos pares ordenados de números naturais (correspondendo à diferença
entre eles). Depois, poderíamos definir os racionais como sendo certos
pares ordenados de inteiros (correspondendo à fração entre inteiros).
Outro procedimento é definir, partindo dos números naturais, diretamente
os racionais como sendo certos pares ordenados de números naturais.
Os inteiros são então definidos como sendo racionais de um certo tipo
\cite{Landau1966}. Em ambos os procedimentos, é possível provar as
propriedades dos números racionais, entre as quais podemos mencionar:
leis comutativas e associativas da adição e da multiplicação, transitividade
da relação maior que (que também é definida em termos dos naturais),
a densidade do conjunto dos racionais, etc.

A discussão informal acima tem o único propósito de evidenciar que
a aritmetização da análise depende, em última instância, dos números
naturais e suas propriedades. O que é mais importante, a autonomia
da análise depende da autonomia da aritmética dos números naturais.
Se os números naturais são dados pela intuição (geométrica ou não),
então a análise é dependente, de alguma forma, da intuição. O projeto
imaginado de aritmetização seria malogrado.

De fato, na \textit{Crítica da Razão Pura} (CPR, 1781/1787), Kant
argumenta a favor da tese segundo a qual a matemática pura é sintética
\textit{a priori}. Isto significa que, embora esta ciência não dependa
de fatos empíricos para provar suas proposições, ela é dependente
das intuições puras de tempo e espaço (veja CPR, B 14-16, A 39/ B
55-6). Portanto, em particular, de acordo com Kant, a aritmética dos
números naturais dependeria de algum tipo de intuição. Não é totalmente
claro se, para Kant, aritmética depende apenas da intuição temporal,
ou se esta ciência também depende da intuição espacial. Em \textit{Prolegomena
to Any Future Metaphysics} (PFM, 1783), Kant escreve:
\begin{quote}
Arithmetic accomplishes its concept of number by the successive addition
of units in time (pág. 32)
\end{quote}
Em CRP (A 142-3 / B 182), há a seguinte passagem: 
\begin{quote}
A imagem pura de todas as quantidades (\textit{quantorum}) para o
sentido externo é o espaço, e a de todos os objetos dos sentidos em
geral é o tempo. O \textit{esquema} puro da quantidade (\textit{quantitatis}),
porém, como \textit{conceito} do entendimento, é o número, que é uma
representação que engloba a adição sucessiva da unidade à unidade
(do homegêneo). Portanto, o número não é mais do que a unidade da
síntese que eu opero entre o diverso de uma intuição homogênea em
geral, pelo fato de eu produzir o próprio tempo na apreensão da intuição
\cite[A142-3/B 182]{Kant1997}
\end{quote}
Em outra passagem da CRP (A 411/B438), ele escreve:
\begin{quote}
Para agora dispormos a tábua das idéias segundo a das categorias,
tomamos em primeiro lugar os dois \textit{quanta} originários de toda
a nossa intuição, o tempo e o espaço. O tempo é em si uma série (\textit{e
a condição formal de todas as séries}\footnote{Meu grifo.}) pelo
que, em relação a um presente dado, podem distinguir-se nele \textit{a
priori} os antecedentia, como condição (o passado) dos \textit{consequentia}
(o futuro)
\end{quote}
De acordo com Robert Hanna (2002):
\begin{quote}
Our pure intuition of the infinite unidirectional successive time-series
supplies a fundamental semantic condition for arithmetic{*} \cite[pág. 336]{Hanna2002}\footnote{\textit{Arithmetic}{*}, de acordo com Hanna, inclui a aritmética recursiva
primitiva e lógica monádica. que é o sistema estudado, ele assume,
por Kant.}
\end{quote}
A progressão infinita que os números naturais formam depende, de alguma
forma, da série infinita dada pela intuição pura do tempo. Não queremos
aqui discutir minuciosamente a filosofia da matemática de Kant, o
que nos levaria muito além do escopo do nosso tema. O que queremos
salientar com as observações acima é a necessidade de explicar, contra
Kant, como a aritmética dos números naturais não depende da intuição.

Um representante do movimento da aritmetização pode escolher dois
tipos de respostas: (1) sustentar que Kant está errado quando afirma
que a aritmética dos números naturais depende da intuição. Neste caso,
ter-se-ia de dar uma explicação de como obtemos conhecimento das proposições
da aritmética sem apelar à intuição; ou (2) tentar-se-ia reduzir a
aritmética dos números naturais a algo mais básico que não apele explícita
ou implicitamente à intuição e, neste caso, afirmar-se-ia que Kant
está errado.

Os dois tipos de resposta acima não são equivalentes. É possível argumentar
a favor do caráter não-intuitivo da aritmética dos números naturais,
sem reduzir estes números (ontologicamente) às coisas mais básicas
que não dependem da intuição.

Durante muito tempo, acreditei que Frege tinha optado pela segunda
posição. Ou seja, acreditava que Frege tentou justificar seu logicismo
por meio da redução dos conceitos aritméticos a conceitos lógicos.
Mas, como tentarei argumentar, as coisas são mais complicadas do que
parece. A definição contextual de Frege parece querer justificar que
nosso conhecimento dos números é não-intuitivo em termos da primeira
posição (não há uma redução, pelo menos ontológica). Mas adiante,
discutirei isso melhor.

\section*{II}

Em 1879, Frege publica o \textit{Begriffsschrift}. Neste livro, temos
a primeira axiomatização do cálculo proposicional, do cálculo de predicados
de primeira ordem e do cálculo de predicados de segunda ordem (este
implicitamente). Na parte I do livro, Frege introduz e explica seus
conceitos lógicos básicos (conteúdo conceitual, negação, implicação,
identidade de conteúdo, função, argumento, quantificação universal,
além de introduzir dois símbolos puramente sintáticos: traço do juízo
e o traço de conteúdo) e sua regra de inferência, modus ponens (Frege
implicitamente assume a regra de substituição).

Na parte II de BS, Frege enuncia os axiomas lógicos expressos por
meio de seus primitivos lógicos e prova usando a regra de inferência
vários teoremas (lógicos). Na parte III da BS, Frege define, a partir
dos seus primitivos lógicos, conceitos aritméticos de hereditariedade,
de ancestral forte, de ancestral fraco e de relação unívoca (função).
E usando os teoremas provados na parte II, junto com as definições
e a regra de inferência, ele prova importantes teoremas sobre a teoria
das sequências. Os dois mais importantes são os teoremas 98 (transitividade
do ancestral) e 133 (se f é funcional, então f conecta quaisquer dois
objetos m e y que estão na relação de ancestralidade f com x)\footnote{Se f é uma função e se y vem depois de x na relação f e m vem depois
de x na relação f, então ou y vem depois de m na relação f ou m vem
depois de y na relação f ou m=y. (conectude)}.

O que Frege faz na BS é uma redução do conceito seguir-em-uma-sequência
ao conceito de implicação (consequência) lógica. Isto significa que
Frege, neste livro, está seguindo o tipo de resposta (2) a Kant (prefácio,
§§ 23-4).

\section*{III}

Em 1884, Frege publica os \textit{Fundamentos da Aritmética}\nocite{Frege1986}.
Nesse livro, Frege busca continuar o trabalho enunciado em BS:
\begin{quote}
Quando me coloquei a questão, a saber, à qual destas duas classes
pertencem os juízos da aritmética, tive de testar primeiro o quão
longe se poderia chegar na aritmética somente por meio de inferências
que dependem das leis do pensamento que transcendem todas as particularidades.
Para isto, eu procedi da seguinte maneira: primeiro eu tentei reduzir
o conceito de ordenação-em-uma-seqüência ao de conseqüência lógica
e disto progredir para o conceito de número \cite[?]{Frege1972}

A aritmética, como eu disse no início, foi o ponto de partida da cadeia
de pensamentos que me levou a minha notação conceitual. Portanto,
eu pretendo aplicá-la, primeiramente, a esta ciência, tentando analisar
seus conceitos em seguida e estabelecer mais profundamente seus teoremas.
Por enquanto, eu apresentei na terceira parte algumas coisas que se
movem nesta direção. Outros prosseguimentos do curso sugerido – a
elucidação do conceito de número, de magnitude e assim por diante
– devem constituir o assunto de outras investigações que pretenderei
produzir imediatamente após este livro \cite[?]{Frege1972}
\end{quote}
Nos \textit{Fundamentos da Aritmética}, Frege propõe então elucidar
o conceito de número cardinal (Anzahl). Podemos dividir este livro
em duas partes: a primeira negativa, Frege discute e refuta uma série
de opiniões referentes à natureza das proposições aritméticas, ao
conceito de número, à noção de cardinalidade. Embora, a primeira parte
seja negativa em caráter, algumas teses Fregeanas estão implicitamente
localizadas nestas seções: por exemplo, nas §§ 5-8 e §18, Frege argumenta
a favor da provabilidade das proposições aritméticas, da possibilidade
de se definir (e obter) os números naturais individuais seguindo Leibniz.
Nas §§9-17, Frege nega que as leis da aritmética sejam verdades indutivas
(contra Mill), e ele apresenta argumentos a favor da analiticidade
(e do caráter lógico) do conceito de número (e da aritmética). Incrivelmente,
o argumento de Frege não é que números são extensões de conceitos
que são objetos lógicos por natureza, mas:
\begin{quote}
O fundamento da aritmética não é mais profundo que o de todo saber
empírico, mais profundo mesmo que o da geometria? As verdades aritméticas
governam o domínio do enumerável. Este é o mais inclusivo; pois não
lhe pertence apenas o efetivamente real, não apenas o intuível, mas
todo o pensável. Não deveriam, portanto as leis dos números manter
com as do pensamento a mais íntima das conexões? \cite[?]{Frege1986}
\end{quote}
Nas §§18-39, Frege discute várias concepções de números defendidas
por seus contemporâneos ou por autores clássicos. O que Frege apresenta
de positivo destas discussões está sumarizado na §45: (1) números
não são abstraídos de coisas na maneira que cor, peso são, número
não é uma propriedade de objeto; (2) número não é algo físico, caso
contrário, em que sentido ele teria uma aplicação universal?; (3)
número não é a resultante de adicionar coisa a coisa; (4) os termos,
conjunto, pluralidade, multiplicidade são inadequados (e vagos) para
usar na definição de número; (5) o conceito de número parece encerrar
em si duas propriedades contraditórias: o um e o muitos.

Como solução as questões levantadas nestas seções, Frege sugere como
resposta que “uma atribuição numérica contém uma predicação de um
conceito”. Isto explicaria porque números diferentes podem ser atribuídos
a uma mesma coleção de coisas, da mesma maneira explicaria a aplicabilidade
universal do número, uma vez que sob conceitos podem cair o que é
físico e mental, o que temporal e espacial e o que é não-temporal
e não-espacial. Também explica como a contradição mencionada em (5)
acima não é, de fato, uma contradição. Conceitos tem um critério de
aplicação e este critério informa a unidade em questão (Aristóteles).
Por outro lado, alguns conceitos (sortais) têm um critério de identidade
que se aplica aos objetos que caem sob eles. Daí a diversidade. Não
há contradição.

\section*{IV}

Acredito que, inicialmente, Frege tentaria, nos \textit{Fundamentos
da Aritmética}, dar uma resposta a Kant em termos de (1) acima. Mas,
quando ele estava escrevendo o livro, ele percebeu que algo não estava
certo e ele tentou corrigir. O resultado é a tensão mencionada. Quero
apresentar alguns fatos para apoiar minha interpretação. Em 1882,
Frege escrevera uma carta a Anton Marty (ou Carl Stumpf) na qual ele
anuncia o seguinte:
\begin{quote}
Agora, eu estou quase completando um livro no qual eu trato do conceito
de número cardinal {[}Anzahl{]} e demonstro que os primeiros princípios
sobre contar os números {[}ersten Sätze über das Zählen der Zahl{]},
que até agora foram considerados, em geral, como axiomas indemonstráveis,
podem ser provados a partir de definições por meio de leis lógicas
somente, de maneira que estes princípios podem ser considerados como
juízos analíticos no sentido de Kant \cite[pág.163]{Frege1976}.
\end{quote}
Este suposto livro foi escrito na notação conceitual, como a seguinte
passagem sugere:
\begin{quote}
Eu me encontro em um círculo vicioso: antes que as pessoas dêem atenção
à minha notação conceitual, elas querem ver o que eu posso fazer com
ela e eu, por minha vez, não posso mostrar isto sem pressupor uma
familiaridade com a minha notação conceitual. Assim, parece que dificilmente
contarei com qualquer leitor para o livro que mencionei no início
{[}da carta{]} \cite[pág. 165]{Frege1976}.
\end{quote}
Alguns dias depois de enviar esta carta, Frege recebera uma correspondência
de Carl Stumpf na qual este último sugere que Frege escreva um livro
na linguagem ordinária e, somente depois, publique o livro na notação
conceitual. Frege parece ter seguido a sugestão de Stumpf e nascia
daí os \textit{Fundamentos da Aritmética}.

Infelizmente, não existe nenhuma cópia desse manuscrito de Frege.
Talvez tenha se perdido junto com os demais na segunda guerra mundial.
Mas, parece ser razoável supor que os \textit{Fundamentos da Aritmética}
tivesse algum tipo de ligação com este manuscrito. Aqui surge a nossa
perplexidade.

\section*{V}

Na introdução dos \textit{Fundamentos}, Frege anuncia pela primeira
vez o Princípio do Contexto, a tese segunda a qual nunca se deve perguntar
pelo significado (Bedeutung) de uma palavra isoladamente, mas apenas
no contexto de uma sentença. Além disso, Frege anuncia outros dois
princípios: separar nitidamente o psicológico do lógico, o subjetivo
do objetivo e nunca perder de vista a distinção entre conceito e objeto.
Como anunciado na introdução, este três princípios são metodológicos
e interdependentes. Quando tomamos o significado dos numerais isoladamente,
poderia ser o caso de ligarmos a este significado uma idéia. Assim,
violaríamos o primeiro princípio. Por outro lado, no contexto de uma
sentença, saberíamos o status ontológico (significado) de uma expressão.
Por exemplo, se em expressão ocorre como o sujeito de uma sentença
que expressa um conteúdo singular (sentença atômica), então tal expressão
significará um objeto. Se outra expressão for um predicado em tal
sentença, então esta expressão significará um conceito.

A segunda ocorrência do Princípio do Contexto é na §60: “é suficiente
que uma proposição como um todo tenha um sentido para que seja conferido
um conteúdo às suas partes” \cite[?]{Frege1986}. Aqui, parece, temos
uma formulação ontológica do Princípio do Contexto, uma vez que é
uma condição para existência de conteúdo das partes da sentença.

A terceira ocorrência do Princípio do Contexto é na §62 (terei mais
a dizer). A quarta ocorrência do princípio encontra-se na §106, que
está intimamente ligada com a §62.

\section*{VI}

Na §62, depois de rejeitar sua primeira tentativa de definição do
conceito de número (uma vez que ela não dava conta do caráter “ontológico”
de números como objetos), Frege evoca o Princípio do Contexto na sua
segunda tentativa de definição:
\begin{quote}
Como nos pode então ser dado um número, se não podemos ter dele nenhuma
representação ou intuição? \textit{Apenas no contexto de uma proposição
as palavras significam algo}. I\textit{mportará portanto definir o
sentido de uma proposição onde ocorra um numeral}. Por enquanto, isto
fica ainda muito a critério de nosso arbítrio. Mas já estabelecemos
que se deva entender pelos numerais objetos independentes. Com isto
temos uma espécie de proposições que devem ter um sentido, proposições
que exprimem um reconhecimento. Se um sinal a deve designar para nós
um objeto, devemos dispor de um critério para decidir, em qualquer
caso, se b é o mesmo que a, ainda que nem sempre sejamos capazes de
aplicá-lo. Em nosso caso, devemos definir o sentido da proposição:
\begin{center}
\textquotedbl{}O número que convém ao conceito F é o mesmo que convém
ao conceito G\textquotedbl{};
\par\end{center}
isto é, devemos reproduzir de outra maneira o conteúdo desta proposição,
sem empregar a expressão \textquotedbl{}o número que convém ao conceito
F\textquotedbl{} \cite[pág. ?]{Frege1986}
\end{quote}
Citando Hume (§63), Frege propõe definir esta identidade em termos
de correlação 1-1 entre os objetos que caem sob F e G. A definição
de correspondência 1-1 pode ser expressa em termos puramente lógicos
(lógica de segunda ordem). E esta definição não pressupõe a noção
de número. Frege adianta uma possível crítica à definição proposta,
a saber, de que ele esteja definindo a identidade para o caso especial
dos números. Ele afirma que o que está sendo feito é usar “o conceito
de identidade, tomado como já conhecido, para chegar naquilo que tem
de ser considerado como idêntico” \cite[pág. ?]{Frege1986}, que
no caso são os números. Há uma interessante passagem na seção 63:
\begin{quote}
Esta parece ser decerto uma espécie muito incomum de definição, a
qual os lógicos ainda não prestaram suficiente atenção; que ela não
é inaudita, alguns exemplos podem mostrá-lo \cite[pág.?]{Frege1986}
\end{quote}
O primeiro exemplo de Frege é a transformação de um juízo que expressa
a relação de paralelismo entre retas em um juízo que expressa a identidade
entre direções de retas. A relação de paralelismo é uma relação de
equivalência e como tal esta relação expressa algo que é comum às
retas quando elas são paralelas. E esta coisa em comum é sua direção.
Mas ‘a direção da reta $a$’ é um nome próprio, então é por isso que
transformamos a relação de paralelismo entre retas em uma identidade
de suas direções (que é o tipo próprio de sentença na qual expressões
podem significar objetos).

Frege sustenta uma prioridade epistêmica do juízo que expressa paralelismo
entre retas em relação ao juízo que expressa a identidade das suas
direções. De acordo com ele, direções são objetos não intuitivos,
dos quais podemos ter algum conhecimento via relações intuitivas dadas
pelas retas. Ele retruca autoridades que defendem que o caminho da
definição é o inverso, isto é, definimos quando retas são paralelas,
dizendo que são aquelas que têm a mesma direção. Para Frege, isso
inverte “verdadeira ordem das coisas”\footnote{Há uma conexão dessa discussão com a tese de doutorado de Frege sobre
geometria projetiva. Veja \citeonline{Wilson1992,Wilson1999}}.

O ponto obscuro é se Frege admitiria uma redução ontológica das direções
de retas a retas paralelas? Parece razoável dizer que não, caso contrário,
a expressão “a direção da reta $a$” não significaria nada dentro
do contexto de uma sentença que expressa de identidade.

Na §65, Frege tenta a seguinte definição:

\begin{center}
\textbf{Princípio de Direção}
\par\end{center}

\begin{center}
$\forall a\forall b[Dir(a)=Dir(b).\equiv.a\Vert b]$
\par\end{center}

Em termos epistemológicos, a proposição ‘reta $a$ é paralela à reta
$b$’ tem prioridade sobre a proposição ‘a direção da reta $a$ =
a direção da reta $b$’, uma vez que temos intuições de retas e da
relação de paralelismo. Mas ontologicamente, assim estou supondo,
nenhuma proposição tem prioridade sobre a outra. Não podemos reduzir
a nossa fala de identidade de direções de retas à fala de paralelismo
entre retas.

Agora, vamos transportar isso para o caso dos números. De acordo com
o que foi mencionado acima, a sentença “Existem tantos $F$s quantos
$G$s” (equinumerosidade, correspondência 1-1) tem o mesmo significado
(conteúdo conceitual) que “o número do conceito $F$ = o número do
conceito $G$”.

\begin{center}
\textbf{Princípio de Hume}
\par\end{center}

\begin{center}
$\forall F\forall G[N_{x}:Fx=N_{x}:Gx.\equiv.F1-1G]$
\par\end{center}

Números são objetos não-intuitivos (argumentado anteriormente nos
\textit{Fundamentos}). A relação de equinumerosidade (correspondência
1-1) é uma relação lógica (redutível aos conceitos lógicos propostos
em BS). Sendo lógica, esta relação é a priori e analítica e independente
da intuição. Esta relação tem uma prioridade epistemológica sobre
a identidade numérica, contudo não temos uma redução ontológica, caso
contrário, não teríamos números como objetos. No Princípio de Hume,
números seriam objetos sui generis, cuja explicação epistemológica
de como obtemos conhecimento sobre eles é dada via nosso conhecimento
lógico de correspondência 1-1. Com o Princípio de Hume, eu creio que
é o primeiro tipo de resposta mencionada acima que Frege desejava
dar a Kant\footnote{Este é o tipo de resposta que Crispin Wright dá ao desafio proposto
por Benacerraf no artigo “Mathematical Truth” de como obtemos conhecimento
de objetos matemáticos, mas não entrarei em detalhes}.

Outro ponto importante a ser salientado é que dada a relação de mesmidade
de conteúdo (ou significado) entre as sentenças que ocorrem no Princípio
de Hume, então se um dois lados da equivalência tiver conteúdo o outro
necessariamente terá e vice-versa (se um lado é verdadeiro, o outro
será). Isso juntamente com a versão ontológica do Princípio do Contexto
na §60, garante a existência dos conteúdos das expressões “a direção
da reta”, “o número do conceito $F$” (quando as sentenças que ocorrem
à direita dos princípios supracitados tiverem).

\section*{VII}

Todavia, Frege descarta esta tentativa de se definir número também.
Ele elabora uma objeção a este tipo de definição que, ele defende,
é fatal. Esta objeção é conhecida como o Problema de Júlio César:
\begin{quote}
Mas, há ainda uma terceira dúvida que pode nos fazer suspeitar da
definição proposta. Na proposição
\begin{center}
‘a direção de $a$ é idêntica à direção de $b$’ 
\par\end{center}
a direção de $a$ desempenha o papel de um objeto, e nossa definição
nos permite um meio de reconhecer este objeto como o mesmo novamente,
no caso deste objeto aparecer repentinamente de alguma outra forma,
por exemplo, como a direção de $b$. Mas, este meio não é suficiente
para todos os casos. Por exemplo, ele não decidirá para nós se a Inglaterra
é a mesma que a direção do eixo da Terra - se eu posso ser desculpado
pelo exemplo que parece ser sem sentido. Naturalmente, ninguém confundirá
a Inglaterra com a direção do eixo da Terra, mas isto não ocorre graças
à nossa definição de direção \cite[§66]{Frege1986}.
\end{quote}
Na maneira como o problema apresentado acima, não fica muito claro,
pelo menos para mim, o que Frege pretende. Ele não articula totalmente
seus argumentos. A questão é que, pela definição de direção de uma
reta $a$, temos

\begin{center}
A direção de uma reta $a$ = a direção da reta $b$ justamente quando
as retas $a$ e $b$ são paralelas
\par\end{center}

\noindent Frege assume o conceito de identidade como dado. Uma vez
que ‘a direção da reta $a$’ desempenha o papel de um objeto, então
podemos, usando o conceito de direção, perguntar se esta direção é
igual à direção de outra reta, digamos, $b$. Essa sentença tem o
mesmo conteúdo que a sentença ``$a$ e $b$ são paralelas''. Se
for o caso dessa retas serem paralelas, então suas direções são idênticas.
Se não for o caso, suas direções são diferentes. Mas poderíamos perguntar,
como na passagem citada, se a Inglaterra é idêntica a direção da reta
$a$. Essa é a questão: se admitirmos que o Princípio da Direção é
capaz de responder isso, então teríamos que a sentença acima tem o
mesmo conteúdo que ``a reta $a$ é paralela à Inglaterra''. Mas
o que isso significa? 

A coisa é ainda mais aguda com o Princípio de Hume. Sentenças da forma:

\begin{center}
o número de $F$ = o número de $G$
\par\end{center}

\noindent seriam respondidas afirmativa ou negativamente, conforme
se existem tantos $F$s quantos $G$s ou não. Agora, pergunte se o
número de $F$ = Júlio César. Se assumirmos que o Princípio de Hume
pode resolver esta identidade, então temos que a sentença acima tem
o mesmo conteúdo que ``Existem tantos $F$ quantos Júlio César''.
Mas esta sentença não tem sentido, uma vez que engloba quantificação
sobre Julio César, mas quantificação é um conceito de segunda ordem
sob o qual caem conceitos de primeira ordem e Júlio César é um objeto.
Assumindo que Princípio de Hume pode dar respostas à identidade acima,
chegamos a conclusão que um dos lados não tem sentido, mas então o
outro (a identidade) também não terá sentido e assim não lhe será
conferido existência a suas partes (§60).

Todavia se os princípios acima não resolvem estas questões de identidades,
qual será o seu conteúdo? As sentenças têm conteúdo? Frege sugere
que elas têm conteúdo (``ninguém irá confundir a direção do eixo
da Terra com a Inglaterra''). Portanto, a questão inicial: ``como
obtemos este conteúdo, principalmente quando um dos objetos é não-espacial
e não-temporal?'' seria respondida por meio da definição contextual
e o princípio do contexto estabelecidos para este propósito. Mas,
uma vez que a definição falha, o que temos?

Há outro ponto a ser mencionado. Obviamente, estas definições não
são definições no sentido estrito (definições nominais): elas não
satisfazem nem o critério de eliminabilidade (o Princípio de Hume
não satisfaz duplamente este critério), nem o de não-criatividade,
nem deveriam, pois neste caso teríamos a redução das expressões que
ocorrem no lado esquerdo às que ocorrem no lado direito. Mas, uma
vez que estes princípios são criativos, os dois lados dos princípios
não podem expressar o mesmo conteúdo. Aqui, estou assumindo que o
que Frege entende por conteúdo é a mesma coisa que ele entende por
conteúdo conceitual em BS. Frege não tem muito a dizer sobre conteúdos,
mas ele afirma quando dois conteúdos conceituais C e C’ são idênticos:
quando, de um mesmo conjunto de sentenças T, C e C’ derivam as mesmas
consequências (em uma mesma linguagem). Mas, no caso do Princípio
de Hume (o mesmo vale para o Princípio de Direção), o conjunto de
consequências de cada lado desse Princípio não é igual (dentro da
lógica de segunda ordem). Do lado esquerdo é possível provar a existência
de números, fato que não pode ser provado das instâncias do lado direito.

\section*{VIII}

Na §68, Frege introduz então as extensões de conceitos. Um fato notável
é que Frege não tem muito a dizer sobre extensões de conceitos (apenas
em duas passagens). Ele assume que já é conhecido o que é uma extensão
de conceito. Além disso, ele define o operador-cardinalidade em termos
de extensão: o número que pertence ao conceito F é a extensão do conceito
‘ser equinumérico ao conceito F’. Uma vez que já sabemos o que é extensão,
então já sabemos que Júlio César não é uma extensão. Além disso, Frege
já está assumindo que existem as extensões que são objetos. A questão
é: qual papel o princípio do contexto desempenha nos Fundamentos?
\citeonline[pp. 201-2]{Dummett1991} sugere um papel metodológico:
o princípio do contexto funciona como um guia para formular uma definição
correta de número cardinal. Mas, como afirmamos anteriormente, o princípio
do contexto parece ter uma interpretação ontológica (§60) e uma epistemológica
(§62). Mas se a definição já nos apresenta o objeto (fora do contexto
de uma sentença), o que fazemos com estas interpretações do princípio?
Por outro lado, \citeonline[pág. 73]{Sluga1980} afirma que o papel
do princípio do contexto é legitimar definições contextuais. Mas Frege
as rejeita. Legitimar o que, se não há nada para legitimar. Estas
são questões difíceis.

\section*{IX}

A minha posição é a seguinte: Frege inicialmente introduziu os números
contextualmente via Princípio de Hume (essa não era uma prática totalmente
nova, Mark Wilson(1991)) no suposto livro mencionada na carta Marty.
Ao aceitar a sugestão de Stumpf, Frege em algum momento se deparou
com a dificuldade lógica em relação a esta definição. Ele então resolve
introduzir as extensões e assumi-las como já conhecidas e resolver
o problema. Isso explica a falta de comprometimento de Frege em relação
às extensões (§107): 
\begin{quote}
Nesta definição, o sentido da expressão ‘extensão de um conceito'
é assumido ser conhecido. Esta maneira de superar a dificuldade não
pode ser esperada encontrar uma aprovação universal e muitos preferiram
outros métodos de remover a dúvida em questão. Não atribuo qualquer
importância decisiva na introdução das extensões de conceitos \cite[pág. 117]{Frege1986}.
\end{quote}
Outro ponto interessante é a estrutura dos esboços de prova das leis
aritméticas (axiomas de Peano). Frege esboça a prova do Princípio
de Hume (§73) e a partir daí, este Princípio é o ator principal das
demais provas (esboço). Tirando as seções 66-68 e algumas outras poucas
passagens, o livro continuaria em ordem. Vale mencionar que no esboço
da prova do Princípio de Hume, Frege implicitamente assume algo como
sua Lei V. A questão é: ele já tinha ideia desta lei quando na época
da publicação dos Fundamentos? Se ele tinha, por que então ele expressa
a falta de comprometimento com as extensões? Por que ele demorou tanto
tempo para publicar as \textit{Leis Básicas da Aritmética} (1893)
já que ele tinha um manuscrito completo? Esse manuscrito é o manuscrito
que ele afirma ter descartado na introdução de GGA? Certamente, com
a introdução dos valores de verdade, a distinção entre sentido e referência
e a introdução dos percursos de valores, Frege teria de começar quase
do zero (o manuscrito foi escrito em 1882).

\bibliographystyle{abntex2-alf}
\addcontentsline{toc}{section}{\refname}\bibliography{mybib1}

\end{document}
