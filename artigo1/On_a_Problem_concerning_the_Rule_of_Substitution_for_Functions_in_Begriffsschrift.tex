%% LyX 2.2.0 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{amssymb}
\usepackage{stackrel}
\usepackage[authoryear]{natbib}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{amsmath}
\usepackage{amsfonts} 
\usepackage{begriff}
\usepackage{proof} 
\usepackage{textcomp} 
\usepackage{supertabular}
\setlength{\BGafterlen}{3pt} 
\setlength{\BGbeforelen}{3pt} 
\setlength{\BGspace}{3pt} 
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{natbib}

\makeatother

\usepackage{babel}
\begin{document}

\title{On a Problem concerning the Rule of Substitution for Functions in
\textit{Begriffsschrift} }

\author{Alessandro Bandeira Duarte\footnote{I would like to thank to Oswaldo Chateaubriand and Gregory Landini that read the paper and made some valuable suggestions.}}
\maketitle
\begin{abstract}
Our aim is to present a problem in \textit{Begriffsschrift}\nocite{Frege1998a}
related to the rule of substitution for functions that leads to the
formation of not-well-formed expression.\\[4pt] \textbf{Keywords}:
Frege, Begriffsschrift, Rule of substitution for functions
\end{abstract}

\section{The Language of Begriffsschrift}

It is a well-known fact that Frege argued for Logicism according to
which the arithmetical concepts could be defined by means of logical
concepts and arithmetical theorems proved from logical axioms and
rules of inference that preserve ``logicality''. 

It was in order to establish his philosophical project that Frege
published his \textit{Begriffsschrift} (\textbf{BS}) in 1879\footnote{In \textbf{BS}, only a little part of Frege's philosophical enterprise
was established. In the part 3 of his booklet, he proved in ``second-order
logic'' that mathematical induction in its general form can be derived
from concepts of strong ancestral and of weak ancestral. See, for
example, \citet[Ch. 12]{Heck2011} and \citet[Ch. 3]{Landini2012}.}. It is a book in which he presented his primitive logical concepts,
introduced symbols that represented these concepts and established
his ``grammatical rules from which we could form other symbols (more
complex), which represented complex concepts. 

Besides, Frege needed establish rules of ``transformation'' that allowed
the passage from a formula to other. These rules of transformation
are the rules of inference of \textbf{BS}. According to Frege, the
only rule used in his little book was \textit{modus ponens}, but
this is not right because he also used the following rules: universal
generalization, confinament of generalization to consequent, uniform
substitution for (symbols of) judgeable contents (propositions) and
uniform substitution for (symbols of) functions. As it will be shown,
there is a problem related to this late rule.

\subsection{The Logical Primitives}

In \textbf{BS}, the logical primitives are the following: implication
(conditional), negation, identity of content, universal generalization\footnote{In addition to them, Frege introduced the notions of function and
argument.}. For each of these primitives, Frege introduced, respectively, the
following symbols:\\[6pt] \textbf{Implication}: $\BGconditional{}{}$\\[6pt]
\textbf{Negation}: $\BGnot$\\[6pt]
\textbf{Identity of Content}: $\equiv$\\[6pt]
\textbf{Universal Generalization}: $\BGall a$\footnote{The higher-order quantification is also expressed in \textbf{BS}: $\BGall f$}\\[6pt] Moreover Frege assume Latin letters as a kind of variable, expressing
universality\footnote{``The symbols customarily used in the general theory of magnitudes fall into two kinds. The first consists of the letters, each of which represents either a number left undetermined or a function left undetermined. This indeterminateness makes it possible to use letter for expression of the general validity of propositions, as in $$(a+b)c=ac+bc\mbox{.}$$ The other kind consists of such symbols as $+$, $-$, $\surd$, $0$, $1$, $2$; each of which has its own specific meaning'' \citep[p. 111]{Frege1972}}. 

There are also in \textbf{BS} two symbols that do not play any semantic
role. These symbols are the judgement stroke and the content stroke
represented, respectively, by symbols: $\BGassert$ and {\renewcommand{\BGcontent}[0]{%
\addtolength{\BGlinewidth}{-\BGafterlen}%
\vrule depth 0pt height \BGthickness width 6mm%
\hskip \BGspace%
}$\BGcontent$}. We do not go in details about the judgement stroke, but it is important
to mention the syntactic role played by the content stroke. 

Firstly, and this is a crucial aspect related to the problem we want
to explain, the content stroke can only be attached to symbols expressing
a judgeable content, that is to say, to contents which are able to
be true or false\footnote{``\textit{The horizontal stroke, \emph{which is part of the symbol}\ {\renewcommand{\BGassert}[0]{%
\addtolength{\BGlinewidth}{-\BGafterlen}%
\addtolength{\BGlinewidth}{-\BGthickness}%
\vrule width \BGthickness height 5pt depth 5pt%
\vrule depth 0pt height \BGthickness width 4mm%
\hskip \BGspace%
}$\BGassert$}, ties the symbols which follow it into a whole; and the assertion, which is expressed by means of the vertical stroke at the left end of the horizontal one, relates to this whole.} Let us call the horizontal stroke the \textit{content stroke}, the vertical one the \textit{judgement stroke}. The content stroke serves to relate any sign to the whole formed by the symbols that follow the stroke. \textit{Whatever follows the content stroke must always have an assertible content}'' \citep[p.112]{Frege1972}.}. 

Secondly, in \textit{BS} the content stroke is used to express different
formulas, without which this would not be possible. To observe this,
let us see how are expressed the formulas (1) $(a\supset(b\supset c))$
and (2) $((a\supset b)\supset c)$ (contemporary language) in the
language of \textbf{BS}:\\

\begin{equation}
\BGconditional{a}{\BGconditional{b}{c}}\tag{1*}
\end{equation} and 

\begin{equation}
\BGconditional{\BGconditional{a}{b}}{c}\tag{2*}
\end{equation} In (1{*}), a conditional stroke \textemdash{} the long vertical stroke
\textemdash{} tie the content strokes of `$b$' and `$c$' and other
conditional stroke tie the content strokes of `$a$' and `$\BGconditional{b}{c}$'.
In (2{*}), the process of formation is different. In first place,
the content strokes of `$a$' and `$b$' are tied by conditional one
and only after the content strokes of `$\BGconditional{a}{b}$' and
`$c$' are tied by the conditional. What allows these different formations
is the content stroke.\footnote{Here `$a$',`$b$' e `$c$' must necessarely express jugdeable contents.}.
Without it, the formulas (1{*}) and (2{*}) would not be expressible
in the language. We would just have something like this: \begin{center} $c$\\[4pt] \rule{0.1mm}{4mm}\\ $b$\\[4pt] \rule{0.1mm}{4mm}\\ $a$\footnote{Frege could have used parenthesis to express those two different formulas, but it is not clear to me how he could express the universality.}\\ \end{center}

It is also important to analyze the identity of content. This primitive
concept represented by `$\equiv$' is applied to the symbols that
express both judgeable and non-judgeable contents. And this is a crucial
aspect of the nature of the problem we are going to analyze in relation
to the rule of substitution for functions\footnote{The following expressions are well-formed in \textbf{BS}: `$(2+2\equiv4)\equiv(3+3\equiv6)$'
and `$2+2\equiv4$'.}.

\subsection{The Inference Rules}

According to Frege, the only inference rule of \textbf{BS} is the
\textit{modus ponens}, which is represented as follows:\begin{center}
$\BGassert\BGconditional{a}{b}$\\ $\BGassert a$\\ \rule{4mm}{0.1mm}\\ $\BGassert b$
\end{center} But he used other rules of inference. For example, the rule of universal
generalization: from $\BGassert\Phi(a)$ infer $\BGassert\BGall a\Phi(\mathfrak{a})$.
Another rule used is the confinament of generalization to consequent:
from $\BGassert\BGconditional{A}{\Phi(a)}$, to infer $\BGassert\BGconditional{A}{\BGall
a\Phi(\mathfrak{a})}$, if the `$a$' does not occur in `$A$'.

Besides these two mentioned, Frege used the rule of substitution for
propositions. For example, from the axiom 1 of \textbf{BS} \begin{equation}
\BGassert\BGconditional{a}{\BGconditional{b}{a}}\tag{Axiom I}
\end{equation} we get the following formula $$\BGassert\BGconditional{a}{\BGconditional{a}{a}}$$
replacing `$a$' for `$b$' in the Axiom 1. We can replace, in a uniform
way, any ``propositional'' letter (Latin letter)\footnote{There is an ambiguity in the use of Latin letters. Sometimes, they
stand for assertible contents, but sometimes they refer to non-assertible
contents. See the formulas 58, 67, 68, 120, 121, 122.} that occurs in a formula by any other letter or well-formed formula
which expresses a judgeable content.

In BS Frege also admitted a rule of substitution for functions. Thus,
for example, from axiom 58 \begin{equation}
\BGassert\BGconditional{\BGall a f(\mathfrak{a})}{f(c)}\tag{Axiom 58}
\end{equation}we can get the following formula $$\BGassert\BGconditional{\BGall a\BGconditional{h(\mathfrak{a})}{g(\mathfrak{a})}} {\BGconditional{h(c)}{g(c)}}$$
by replacing the function $\BGconditional{h(\Gamma)}{g(\Gamma)}$
for $f(\Gamma)$, where `$\Gamma$' represents the place of argument.

\section{The Problem}

The problem we have rediscovered is related to the following replacement
for functions that Frege made in \textbf{BS}: to substitute $\Gamma$
for $f(\Gamma)$\footnote{`$\Gamma$' would express a function whose value is its own argument.}. .
This kind of substitution is used, for example, in the proof of Theorem
75 of \textbf{BS}. Frege defines the notion of hereditary: \begin{equation} \Vdash\left[\BGbracket{\BGall d\BGconditional{F(\mathfrak{d})}{\BGall a\BGconditional{f(\mathfrak{d},\mathfrak{a})}{F(\mathfrak{a})}}}\equiv\ \stackrel[\alpha]{\delta}{\rule[0.01mm]{0.1mm}{3mm}} \Big(^{F(\alpha)}_{f(\delta,\alpha)}\right]\tag{Her} \end{equation}
From this definition, which is introduced through the identity of
content, Frege wants to obtain the following conditional $$\BGassert\BGconditional{\BGall d\BGconditional{F(\mathfrak{d})}{\BGall a\BGconditional{f(\mathfrak{d},\mathfrak{a})}{F(\mathfrak{a})}}}{\stackrel[\alpha]{\delta}{\rule[0.01mm]{0.1mm}{3mm}} \Big(^{F(\alpha)}_{f(\delta,\alpha)}}$$
For this he used the axiom 52 of \textbf{BS} \begin{equation} \BGassert\BGconditional{(c\equiv d)}{\BGconditional{f(c}{f(d)}}\tag{Axiom 52} \end{equation}
and made the following substitutions:: `$\BGall d\BGconditional{F(\mathfrak{d})}{\BGall a\BGconditional{f(\mathfrak{d},\mathfrak{a})}{F(\mathfrak{a})}}$'
for `$c$', `$\stackrel[\alpha]{\delta}{\rule[0.01mm]{0.1mm}{3mm}} \Big(^{F(\alpha)}_{f(\delta,\alpha)}$'
for `$d$' and `$\Gamma$' for `$f(\Gamma)$'. With this, we get the
formula \begin{equation}
\BGassert\BGconditional{\left[\BGbracket{\BGall d\BGconditional{F(\mathfrak{d})}{\BGall a\BGconditional{f(\mathfrak{d},\mathfrak{a})}{F(\mathfrak{a})}}}\equiv\ \stackrel[\alpha]{\delta}{\rule[0.01mm]{0.1mm}{3mm}} \Big(^{F(\alpha)}_{f(\delta,\alpha)}\right]}{\BGconditional{\BGall d\BGconditional{F(\mathfrak{d})}{\BGall a\BGconditional{f(\mathfrak{d},\mathfrak{a})}{F(\mathfrak{a})}}}{\stackrel[\alpha]{\delta}{\rule[0.01mm]{0.1mm}{3mm}} \Big(^{F(\alpha)}_{f(\delta,\alpha)}}}_{.}\tag{T}
\end{equation} Applying \textit{modus ponens} between (Her) and (T), we arrive at
the desired conditional\footnote{All the definitions of \textbf{BS} have the following form: $\Vdash(A\equiv B)$, where `$A$' is the \textit{definiens} and `$B$', the \textit{definiendum}. From his definitions and using the axiom 52, already mentioned, and theorem 57 --- $\BGassert\BGconditional{(c\equiv d)}{\BGconditional{f(d}{f(c)}}$ ---, Frege intent to get the conditionals: $\BGassert\BGconditional{A}{B}$ e $\BGassert\BGconditional{B}{A}$. This is a method that Frege maintained in \textit{Grundgesetze der Arithmetik}.\nocite{Frege1998b} The substitution for functions in the example above has the aim to eliminate the symbol `$f( )$'. In this case, the symbol `$\equiv$' would behave like a kind of equivalence. Nevertheless, we can not assume that `$\equiv$' has exactly the meaning of the equivalence, because teh following is not provable in \textbf{BS}: $ \BGassert\BGconditional{\BGconditional{a}{b}}{ \BGconditional{\BGconditional{b}{a}} {(a\equiv b)}}$. As far as I know, \citep{Landini1996} was the first to realize this. See also \citep{Chateaubriand2001} and \citep{Duarte2009}.}

As already mentioned, the problem lies in replacing of `$\Gamma$'
for `$f(\Gamma)$'. This substitution is valid only when `$c$' and
`$d$' are replaced by judgeable contents in the axiom 52. If `$c$'
e `$d$' are substituted by non-judgeable contents, say, `$x$' e
`$y$', so the following instance of axiom 52 $$\BGassert\BGconditional{(x\equiv y)}{\BGconditional{x}{y}}$$
is not true, because `{\renewcommand{\BGcontent}[0]{%
\addtolength{\BGlinewidth}{-\BGafterlen}%
\vrule depth 0pt height \BGthickness width 6mm%
\hskip \BGspace%
}$\BGcontent x$}' and `{\renewcommand{\BGcontent}[0]{%
\addtolength{\BGlinewidth}{-\BGafterlen}%
\vrule depth 0pt height \BGthickness width 6mm%
\hskip \BGspace%
}$\BGcontent y$}' are not well-formed. Remember that the stroke of content can only
be attached to symbols that express judgeable contents, but we assume
that '$x$' and $`y $ are non-judgeable contents.

One possible answer to this problem could be: in \textbf{BS} there
are only symbols that express judgeable contents. However, this conflicts
with the definitions of strong ancestral, weak ancestral and functional
relation\footnote{In fact, Frege is very much explicit about the existence of non-judgeable contents in \textbf{BS}: ``\textit{If, in an expression (whose content need not be assertible), a simple or complex symbols occur in one or more places and we imagine it as repleceable by another [symbol] (but the same one each time) at all or some of these places, then we call the part of expression that shows itself invariant [under such replacement] a function and the repleceable part its argument} \citep[p. 127]{Frege1972} .}\footnote{By via of email, Gregory Landini mentioned that the following substitution
in the axiom 52 would result in the same problem I have pointed out
here, namely: `$\BGconditional{c}{\Gamma}$' for `$f(\Gamma)$'. From
this substitution, it is obtained the formula:\\$$\BGassert\BGconditional{(c\equiv d)}{\BGconditional{\BGconditional{c}{c}}{\BGconditional{c}{d}}}$$

But, the formula $$\BGassert\BGconditional{c}{c}$$ is a theorem of
\textbf{BS}, so applying \textit{modus ponens}, we get:\\$$\BGassert\BGconditional{(c\equiv d)}{\BGconditional{c}{d}}_{.}$$}. Moreover, Frege had to introduce individual constants (numerals)
representing objects (non-judgeable contents) to his conceptual notation.
With that, the mentioned problem would take place, because the substitution
for functions should preserve `logicality ', but this would not be
possible in all cases, as sometimes we would have the formation of
not-well-formed expressions.\footnote{In \textit{Die Grundlagen de Arithmetik}\nocite{Frege1988}, Frege define the number 0 as the number belonging to the concept ``being  different from itself''. If we introduce the symbol $N_x...x...$ by expressing the function ``the number belonging to...'', then the defintion would have the following form in textit{BS}: $\Vdash (0\equiv N_x(x\neq x))$. Well, but then why we could not apply the substitution of `$\Gamma$' for $f(\Gamma)$ in axiom 52, where `$c$' would be replaced by `$0$' and `$d$' by `$N_x(x \neq x)$'? The substitution rule for functions could not universally applied.}\footnote{Probably Frege became aware of it. Indeed, the introduction of horizontal, the introduction of truth values as objects and the distinction between sense and reference play a formal role and avoid serious problems that occur in the logical system of \textbf{BS}. See \citet{Duarte2009} and \citet{Landini2012}}.

\bibliographystyle{elsarticle-harv}
\bibliography{mybibio}

\end{document}
